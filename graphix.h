
const byte 
  mapping[] = {
0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
0x1f,0x1e,0x1d,0x1c,0x1b,0x1a,0x19,0x18,0x17,0x16,0x15,0x14,0x13,0x12,0x11,0x10,
0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
0x3f,0x3e,0x3d,0x3c,0x3b,0x3a,0x39,0x38,0x37,0x36,0x35,0x34,0x33,0x32,0x31,0x30,

0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,
0x5f,0x5e,0x5d,0x5c,0x5b,0x5a,0x59,0x58,0x57,0x56,0x55,0x54,0x53,0x52,0x51,0x50,
0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,
0x7f,0x7e,0x7d,0x7c,0x7b,0x7a,0x79,0x78,0x77,0x76,0x75,0x74,0x73,0x72,0x71,0x70,

0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,
0x9f,0x9e,0x9d,0x9c,0x9b,0x9a,0x99,0x98,0x97,0x96,0x95,0x94,0x93,0x92,0x91,0x90,
0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,
0xbf,0xbe,0xbd,0xbc,0xbb,0xba,0xb9,0xb8,0xb7,0xb6,0xb5,0xb4,0xb3,0xb2,0xb1,0xb0,

0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,
0xdf,0xde,0xdd,0xdc,0xdb,0xda,0xd9,0xd8,0xd7,0xd6,0xd5,0xd4,0xd3,0xd2,0xd1,0xd0,
0xe0,0xe1,0xe2,0xe3,0xe4,0xe5,0xe6,0xe7,0xe8,0xe9,0xea,0xeb,0xec,0xed,0xee,0xef,
0xff,0xfe,0xfd,0xfc,0xfb,0xfa,0xf9,0xf8,0xf7,0xf6,0xf5,0xf4,0xf3,0xf2,0xf1,0xf0
  };


void dot(int y, dword c) {
  y = mapping[y];
  pixels.put(y,c);
}

void putPixel(byte x,byte y,dword c) {
  if (c == None) {
    return;
  }
  if (x>=0 && x<XL && y>=0 && y<YL) {
    dot((x<<4) + y,c);
  }
}

void putBmp(byte x, byte y, dword*b) {
  int i = 0;
  for (int _y=0;_y<b[1];_y++) {
    for (int _x=0;_x<b[0];_x++) {
      dword c = b[2+i++];
      if (c!=0) {
        putPixel(_x+x,_y+y,c);   
      }
    }
  }
}

void cls(dword c) {
  if (c < 0) {
    return;
  }
  for (dword i = 0; i<LEDS; i++) {
    dot(i,c);
  }
}

void sync() {
  pixels.show();
  cls(0);  
  t++;  
}

void line(int x0, int y0, int x1, int y1,dword c) {
  int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
  int err = (dx>dy ? dx : -dy)/2, e2;
  for(;;) {
    putPixel(x0,y0,c);
    if (x0==x1 && y0==y1) {
      break;
    }
    e2 = err;
    if (e2 >-dx) { 
      err -= dy; x0 += sx; 
    }
    if (e2 < dy) { 
      err += dx; y0 += sy; 
    }
  }
}

void circle(int x,int y, int r,dword c) {
  float a,b,p = (2 * 3.1415) / 360;
  dword C;
//  putPixel(x,y,c);
  for (int i = 0; i < 360; i+=10) {  
    int R = r;//analogRead(14);
    a = R * sin(i * p) + x;
    b = R * cos(i * p) + y;
    if (R < 0) {
      C = !c;
    } else {
      C = c;
    }
//    line(x,y,a,b,C);
    putPixel(a,b,c);
  }
}

class Vector {
  private:
    double _dx;
    double _dy;
  public:
    Vector(double __dx,double __dy) { dx(__dx); dy(__dy); };
    ~Vector(){};

    double dx() { return _dx; };
    double dy() { return _dy; };
    void dx(double dx) { _dx = dx; };
    void dy(double dy) { _dy = dy; };
};

class Ball {
  private:
    int _x;
    int _y;
    dword _c;
  public:
    Ball(int __x,int __y,dword __color) { x(__x); y(__y); color(__color); };
    ~Ball(){};

    int x() { return _x; };
    int y() { return _y; };
    dword color() { return _c; };
    void x(int x) { _x = x; };
    void y(int y) { _y = y; };
    void color(dword color) { _c = color; };
    void put() { 
      putPixel(_x,_y,_c); 
    };
};

class Box {
  private:
    int _x;
    int _y;
    int _xl;
    int _yl;
    dword _c;
  public:  
    Box(int __x,int __y,int __xl,int __yl,dword __color) { x(__x); y(__y); xl(__xl); yl(__yl); color(__color); };
    ~Box() {};

    int x() { return _x; };
    int y() { return _y; };
    int xl() { return _xl; };
    int yl() { return _yl; };
    dword color() { return _c; };
    void x(int x) { _x = x; };
    void y(int y) { _y = y; };
    void xl(int xl) { _xl = xl; };
    void yl(int yl) { _yl = yl; };
    void color(dword color) { _c = color; };
    void put() {
      for (int y=0;y<_yl;y++) {
        for (int x=0;x<_xl;x++) {
          putPixel(_x+x,_y+y,_c);   
        }
      }
    };
};


class Pong {
  private:
    Ball*_ball;
    Box*_frame;
    Vector*_vector;
    int xmin;
    int xmax;
    int ymin;
    int ymax;
    double x;
    double y;
    double dx;
    double dy;
  public:
    Pong( Box*f, Ball*b,Vector*v) { 
      _ball = b; 
      _frame = f;
      _vector = v;
      x = b->x();
      y = b->y();
      dx = v->dx();
      dy = v->dy();
      xmin = f->x();
      xmax = xmin+f->xl();
      ymin = f->y();
      ymax = ymin+f->yl();
    };
    
    ~Pong() {
    };
    
    Ball*ball() { 
      return _ball; 
    };
    
    Box*frame() { 
      return _frame; 
    };
    
    void frame(Box*f) { 
      _frame = f; 
    };  

    void bounce() {
      y += dy;
      if (y>=ymax || y<ymin) {
        dy=-dy;
        y+=dy*2;
      }
      x+=dx;
      if (x>=xmax || x<xmin) { 
        dx=-dx;
        x+=dx*2;
      }
      _ball->x(x);
      _ball->y(y);
      _ball->put();
    };
};
