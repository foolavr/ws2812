#define h0 0b0111101101101111
#define h1 0b0100100100100100
#define h2 0b0111001111100111
#define h3 0b0111100111100111
#define h4 0b0100100111101101
#define h5 0b0111100111001111
#define h6 0b0111101111001111
#define h7 0b0100100100100111
#define h8 0b0111101111101111
#define h9 0b0111100111101111
#define ha 0b0101101111101110
#define hb 0b0011101011101011
#define hc 0b0110001001001110
#define hd 0b0011101101101011
#define he 0b0111001111001111
#define hf 0b0001001111001111

    byte
      hexmapping[] = {
        0,0,1,0,2,0,
        0,1,1,1,2,1,
        0,2,1,2,2,2,
        0,3,1,3,2,3,
        0,4,1,4,2,4
    };
    
    uint16_t
      hexDigits[] = {
        h0,h1,h2,h3,
        h4,h5,h6,h7,
        h8,h9,ha,hb,
        hc,hd,he,hf
    };


class Char {
  private:
    int _x;
    int _y;
    dword _c;
    byte _a;
  public:
    Char(int __x,int __y,dword __c,byte __a) { x(__x); y(__y); color(__c); a(__a);};
    ~Char(){};
    int x() { return _x; };
    int y() { return _y; };
    void x(int x) { _x = x; };
    void y(int y) { _y = y; };
    dword color() { return _c; };
    void color(dword color) { _c = color; };
    byte a() { return _a; };
    void a(byte a) { _a= a;};
    void put() {
      byte x,y;
      uint16_t a = hexDigits[_a&0xf];
      for (byte i = 0; i < 15; i++) {
        x = hexmapping[i<<1];
        y = hexmapping[(i<<1) + 1];
        if ((a>>i)&1) {
          putPixel(x+_x,y+_y,_c); 
        } /*else {
          putPixel(x+_x,y+_y,0); 
        }*/
      }
    };
};
