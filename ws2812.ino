#define dword uint32_t

#include "colors.h"
#include "ws2812.h"

#define PIN 2 
#define BPP 1

#define XL 16
#define YL 16
#define LEDS (XL*YL)

ws2812 pixels(LEDS, PIN, BPP);
int t;

#include "graphix.h"
#include "char.h"


int32_t pal[] {
0x000001,//
0x00000F,//G
0x000F00,//R
0x0F0000,//B
0x000F0F,//Y
0x0F0F00,//M
0x0F000F,//C
0x0F0F0F //W
};

#define R 2
dword
 coeur[] = {
  7,6,
  0,R,R,0,R,R,0,
  R,0,0,R,0,0,R,
  R,0,0,0,0,0,R,
  0,R,0,0,0,R,0,
  0,0,R,0,R,0,0,
  0,0,0,R,0,0,0
 };


Box outframe(0,0,16,16,1);
Box inframe(1,1,14,14,0);
Box chargeframe(5,0,1,5,4);
Pong redpong(&outframe,new Ball(2,2,5),new Vector(0.03,0.08));
Pong greenpong(&outframe,new Ball(2,2,6),new Vector(-0.05,0.037));
Pong bluepong(&outframe,new Ball(2,2,7),new Vector(0.0722,0.195));
Pong purplepong(&outframe,new Ball(2,2,2),new Vector(-0.0722,0.195));
Pong yellowpong(&inframe,new Ball(2,2,1),new Vector(0.0995,0.061));
Char test0(2,2,1,0x6);
Char test1(6,2,2,0x6);
Char test2(10,2,3,0x6);

void matrix() {
  outframe.color(1);
  outframe.put();
  inframe.put();
  redpong.bounce();
  greenpong.bounce();
  bluepong.bounce();
  purplepong.bounce();
  yellowpong.bounce();
  circle(7,7,3-((t>>4)&3),3);
  test0.a((t>>11));
  test1.a((t>>7));
  test2.a((t>>3));
  test0.put();
  test1.put();
  test2.put();
  putBmp(4,8,coeur);
  sync();
}  


void setup() {
  if (BPP == 1) {
    pixels.setPalette(pal,8*4);
  }
}

void loop() {
  t++;
  matrix();
  /*
  pixels.clear(); 
  if (BPP == 3) {
    pixels.putRGB(t&0xff, pal[(t>>8)&0x7]);
  } else {
    pixels.put(t&0xff,(t>>8) & 0x7);
  }
  if ((t&0xf) == 0) {
    pal[0] = pal[0] << 8;
    if (pal[0] == 0) {
      pal[0] = 0x000001;
    }
  }
  pixels.show();
  */  
}
