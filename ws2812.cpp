/*
this lib will be designed to use preallocated ram in order to get the real ram useage count at compile time.
then, the coder can see the ram overflow.

for the moment, this lib can use a 8 bit color in a palette, and then divide the ram useage by 3 for the frameBuffer

this lib will give a VGA like feature to let the use of x86 VGA techniques and memory optimisations
for example, a 16 color palette with 4 bits per pixel. a 2 color palette with 1 bit per pixel, and so on.

*/

#include "ws2812.h"

ws2812::ws2812(uint16_t n, uint16_t p, uint8_t _bpp) {
  pixels = NULL;
  bpp = _bpp;
  updateLength(n);
  setPin(p);
}

ws2812::ws2812() {
  pixels = NULL;
  bpp = 0;
  numLEDs = 0;
  numBytes = 0;
  pin = -1;
}

ws2812::~ws2812() {
  free(pixels);
  if(pin >= 0) {
	  pinMode(pin, INPUT);
  }
}

void ws2812::setPin(uint16_t p) {
  pin = p;
  if (p < 0) {
    return;
  }
  pinMode(p, OUTPUT);
  digitalWrite(p, LOW);
  port = portOutputRegister(digitalPinToPort(p));
  pinMask = digitalPinToBitMask(p);
}

void ws2812::updateLength(uint16_t n) {
  free(pixels);
  numBytes = n*bpp;
  if((pixels = (uint8_t *)malloc(numBytes))) {
    memset(pixels, 0, numBytes);
    numLEDs = n;
  } else {
    numLEDs = numBytes = 0;
  }
}

void ws2812::setPalette(uint32_t * p,uint16_t c) {
  isPalette = true;
  bpp = 1;
  palette = p;
  colors = c;
}

void ws2812::put( uint16_t n, uint8_t c) {
  if(n < numLEDs) {
    pixels[n] = c;           
  }
}

void ws2812::put888( uint16_t n, uint8_t r, uint8_t g, uint8_t b) {
  if (isPalette) {
    put(n,r);
    return;
  }
  if(n < numLEDs) {
    uint8_t *p = &pixels[n * 3];   
    p[0] = r;          
    p[1] = g;
    p[2] = b;
  }
}

void ws2812::putRGB(uint16_t n, uint32_t c) {
  if (isPalette) {
    put(n,c);
    return;
  }
  if(n < numLEDs) {
    uint8_t *p = &pixels[n * 3];
    p[0] = (uint8_t)(c >> 16);
    p[1] = (uint8_t)(c >>  8);
    p[2] = (uint8_t)c;
  }
}

void ws2812::clear(void) {
  memset(pixels, 0, numBytes);
}

void ws2812::show(void) {
  if (pin < 0) {
    return;
  }
  if(!pixels) {
    return;
  }
  volatile uint16_t i;
  volatile uint8_t *ptr = pixels, b, hi, lo;
  
// 20 inst. clocks per bit: HHHHHxxxxxxxxLLLLLLL
// ST instructions:         ^   ^        ^       (T=0,5,13)
  volatile uint8_t next, bit;
  hi   = *port |  pinMask;
  lo   = *port & ~pinMask;
  next = lo;
  bit  = 8;

  uint32_t Pal[] = {0,};

  noInterrupts();
  if (isPalette) {
    i = numBytes; 
    volatile uint8_t c;
    volatile uint16_t p;    
    while (i != 0) {
      p = *ptr;
      for (c = 0; c < 3; c++) {
        bit = 8;
        b = ((uint8_t*)(palette))[(p<<2)+c];
        asm volatile(
         "head201:"                   "\n\t" // Clk  Pseudocode    (T =  0)
          "st   %a[port],  %[hi]"    "\n\t" // 2    PORT = hi     (T =  2)
          "sbrc %[byte],  7"         "\n\t" // 1-2  if(b & 128)
           "mov  %[next], %[hi]"     "\n\t" // 0-1   next = hi    (T =  4)
          "dec  %[bit]"              "\n\t" // 1    bit--         (T =  5)
          "st   %a[port],  %[next]"  "\n\t" // 2    PORT = next   (T =  7)
          "mov  %[next] ,  %[lo]"    "\n\t" // 1    next = lo     (T =  8)
          "breq nextbyte201"          "\n\t" // 1-2  if(bit == 0) (from dec above)
          "rol  %[byte]"             "\n\t" // 1    b <<= 1       (T = 10)
          "rjmp .+0"                 "\n\t" // 2    nop nop       (T = 12)
          "nop"                      "\n\t" // 1    nop           (T = 13)
          "st   %a[port],  %[lo]"    "\n\t" // 2    PORT = lo     (T = 15)
          "nop"                      "\n\t" // 1    nop           (T = 16)
          "rjmp .+0"                 "\n\t" // 2    nop nop       (T = 18)
          "rjmp head201"              "\n\t" // 2    -> head20 (next bit out)
         "nextbyte201:"               "\n\t" //                    (T = 10)
          "st   %a[port], %[lo]"     "\n\t" // 2    PORT = lo     (T = 15)
    
          : [port]  "+e" (port),
            [byte]  "+r" (b),
            [bit]   "+r" (bit),
            [next]  "+r" (next)
          : [hi]     "r" (hi),
            [lo]     "r" (lo)
        );
      }
      ptr++;
      i--;
    }
  } else {
    i = numBytes;
    while (i != 0) {
      bit = 8;
      b = *ptr;
      asm volatile(
       "head20:"                   "\n\t" // Clk  Pseudocode    (T =  0)
        "st   %a[port],  %[hi]"    "\n\t" // 2    PORT = hi     (T =  2)
        "sbrc %[byte],  7"         "\n\t" // 1-2  if(b & 128)
         "mov  %[next], %[hi]"     "\n\t" // 0-1   next = hi    (T =  4)
        "dec  %[bit]"              "\n\t" // 1    bit--         (T =  5)
        "st   %a[port],  %[next]"  "\n\t" // 2    PORT = next   (T =  7)
        "mov  %[next] ,  %[lo]"    "\n\t" // 1    next = lo     (T =  8)
        "breq nextbyte20"          "\n\t" // 1-2  if(bit == 0) (from dec above)
        "rol  %[byte]"             "\n\t" // 1    b <<= 1       (T = 10)
        "rjmp .+0"                 "\n\t" // 2    nop nop       (T = 12)
        "nop"                      "\n\t" // 1    nop           (T = 13)
        "st   %a[port],  %[lo]"    "\n\t" // 2    PORT = lo     (T = 15)
        "nop"                      "\n\t" // 1    nop           (T = 16)
        "rjmp .+0"                 "\n\t" // 2    nop nop       (T = 18)
        "rjmp head20"              "\n\t" // 2    -> head20 (next bit out)
       "nextbyte20:"               "\n\t" //                    (T = 10)
        "st   %a[port], %[lo]"     "\n\t" // 2    PORT = lo     (T = 15)
  
        : [port]  "+e" (port),
          [byte]  "+r" (b),
          [bit]   "+r" (bit),
          [next]  "+r" (next)
        : [hi]     "r" (hi),
          [lo]     "r" (lo)
  	  );
      ptr++;
      i--;
    }
  }
  interrupts();
}
