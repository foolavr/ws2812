#ifndef ws2812_H
#define ws2812_H

#include <Arduino.h>

class ws2812 {

 public:

  ws2812(uint16_t n, uint16_t pin=2, uint8_t bpp=3);
  ws2812();
  ~ws2812();

  void              updateLength(uint16_t n);
  void              setPin(uint16_t p);
  void              setPalette(uint32_t * p, uint16_t c);

  void              put888(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
  void              putRGB(uint16_t n, uint32_t c);
  void              put(uint16_t n, uint8_t c);

  void              clear(void);

  void              show(void);

 protected:
 
  bool              isPalette = false;
  uint32_t          *palette;
  uint16_t          colors;

  uint8_t           bpp;
  uint16_t          numLEDs;    
  uint16_t          numBytes;   

  //uint8_t           *frameBuffer;
  uint8_t           *pixels;     

  int16_t           pin;        
  volatile uint8_t  *port;       
  uint8_t           pinMask;    
};

#endif // ws2812_H
